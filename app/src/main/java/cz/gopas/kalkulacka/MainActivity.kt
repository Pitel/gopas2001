package cz.gopas.kalkulacka

import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager

class MainActivity : AppCompatActivity() {
	private companion object {
		private val TAG = MainActivity::class.java.simpleName
		private const val RESULT_KEY = "result"
	}

	init {
	    FragmentManager.enableDebugLogging(BuildConfig.DEBUG)

		StrictMode.setThreadPolicy(
			StrictMode.ThreadPolicy.Builder()
				.detectAll()
				.penaltyLog()
				.build()
		)
		StrictMode.setVmPolicy(
			StrictMode.VmPolicy.Builder()
				.detectAll()
				.penaltyLog()
				.build()
		)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

//		if (savedInstanceState == null) {
//			supportFragmentManager.commit {
//				add(R.id.container, CalcFragment())
//			}
//		}

		"onCreate".let { str ->
			Toast.makeText(this, str, Toast.LENGTH_SHORT).show()
			Log.d(TAG, str)
		}

		intent.dataString?.let { Log.d(TAG, it) }
	}

	/*
	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putCharSequence(RESULT_KEY, result.text)
	}

	override fun onRestoreInstanceState(savedInstanceState: Bundle) {
		super.onRestoreInstanceState(savedInstanceState)
		result.text = savedInstanceState.getCharSequence(RESULT_KEY)
	}
	 */

	override fun onStart() {
		super.onStart()
		Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onStart")
	}

	override fun onResume() {
		super.onResume()
		Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onResume")
	}

	override fun onPause() {
		Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onPause")
		super.onPause()
	}

	override fun onStop() {
		Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onStop")
		super.onStop()
	}

	override fun onDestroy() {
		Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onDestroy")
		super.onDestroy()
	}
}