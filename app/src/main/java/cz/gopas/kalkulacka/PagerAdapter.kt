package cz.gopas.kalkulacka

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when(position) {
        0 -> CalcFragment()
        1 -> HistoryFragment()
        else -> TODO("New fragment")
    }
}