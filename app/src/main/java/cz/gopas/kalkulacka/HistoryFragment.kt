package cz.gopas.kalkulacka

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment: Fragment(R.layout.fragment_history) {
    private val viewModel by lazy { ViewModelProvider(requireActivity())[HistoryViewModel::class.java] }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler.setHasFixedSize(true)
        val adapter = HistoryAdapter { viewModel.selected.value = it }
        recycler.adapter = adapter

        //adapter.submitList(List(1000) { HistoryEntity(it.toFloat(), it.toLong()) })
        viewModel.history.getHistory().observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }
}