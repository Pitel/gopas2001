package cz.gopas.kalkulacka

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room

class HistoryViewModel(application: Application): AndroidViewModel(application) {
    val history = Room.databaseBuilder(application, HistoryDatabase::class.java, "history")
        .build().historyDao()
    val selected = MutableLiveData<Float>()
}