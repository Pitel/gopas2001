package cz.gopas.kalkulacka

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async

class CalcViewModel(application: Application) : AndroidViewModel(application) {
    private companion object {
        private const val ANS_KEY = "ans"
    }

    private val _result = MutableLiveData<Float>()
    val result: LiveData<Float> = _result

    private val prefs =
        viewModelScope.async(IO) { PreferenceManager.getDefaultSharedPreferences(application) }

    suspend fun getAns() = prefs.await().getFloat(ANS_KEY, 0f)
    private suspend fun setAns(value: Float) = prefs.await().edit { putFloat(ANS_KEY, value) }

    suspend fun calc(a: Float, b: Float, @IdRes operation: Int) {
        _result.postValue(
            when (operation) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }.also { setAns(it) }
        )
    }
}