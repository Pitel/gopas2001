package cz.gopas.kalkulacka

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_pager.*

class PagerFragment: Fragment(R.layout.fragment_pager) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = PagerAdapter(this)
        pager.adapter = adapter

        TabLayoutMediator(tabs, pager) { tab, position ->
            tab.text = when(position) {
                0 -> getString(R.string.app_name)
                1 -> getString(R.string.history)
                else -> position.toString()
            }
        }.attach()

    }
}