package cz.gopas.kalkulacka

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.lifecycle.whenCreated
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_calc.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class CalcFragment : Fragment(R.layout.fragment_calc) {

    init {
        setHasOptionsMenu(true)
    }

    private companion object {
        private val TAG = CalcFragment::class.java.simpleName
        private const val PERMISSION_REQUSET_ID = 8776
    }

    private val viewModel by lazy { ViewModelProvider(this)[CalcViewModel::class.java] }
    private val historyViewModel by lazy { ViewModelProvider(requireActivity())[HistoryViewModel::class.java] }

//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View = inflater.inflate(R.layout.fragment_calc, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        calc.setOnClickListener { viewLifecycleOwner.lifecycleScope.launch { calc() } }
        viewModel.result.observe(viewLifecycleOwner) {
            result.text = it.toString()
            share.isVisible = true
            lifecycleScope.launch {
                historyViewModel.history.insert(HistoryEntity(it))
            }
        }
        historyViewModel.selected.observe(viewLifecycleOwner) {
            if (it != null) {
                aText.editText!!.setText(it.toString())
                historyViewModel.selected.value = null
            }
        }
        share.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, result.text))
            startActivity(intent)
        }
        ans.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                aText.editText!!.setText(viewModel.getAns().toString())
            }
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUSET_ID)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUSET_ID -> Log.d(TAG, "${permissions.first()} = ${grantResults.first()}")
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private suspend fun calc() {
        Log.d(TAG, "Calc!")

        fun TextInputLayout.toFloat() = editText!!.text.toString().toFloat()

        try {
            val a = aText.toFloat()
            val b = bText.toFloat()
            if (b == 0f && operation.checkedRadioButtonId == R.id.div) {
                ZeroDialog().show(childFragmentManager, null)
            } else {
                viewModel.calc(a, b, operation.checkedRadioButtonId)
            }
        } catch (t: Throwable) {
            Log.w(TAG, t)
            result.text = t.message
            share.isInvisible = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            requireActivity().supportFragmentManager.commit {
                replace(R.id.container, AboutFragment("Honza"))
                addToBackStack(null)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}