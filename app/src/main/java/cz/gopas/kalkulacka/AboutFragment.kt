package cz.gopas.kalkulacka

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment(): Fragment(R.layout.fragment_about) {
    private companion object {
        private val TAG = AboutFragment::class.java.simpleName
        private const val NAME_KEY = "name"
    }

    constructor(name: String): this() {
        arguments = bundleOf(NAME_KEY to name)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        about.text = getString(R.string.about, arguments!!.getString(NAME_KEY))
    }
}