package cz.gopas.kalkulacka

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {
    @Query("SELECT * FROM HistoryEntity ORDER BY id DESC")
    fun getHistory(): LiveData<List<HistoryEntity>>

    @Insert
    suspend fun insert(entity: HistoryEntity)
}